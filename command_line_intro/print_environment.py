#!/usr/bin/python3

import os

for key, value in os.environ.items():
  print("VARIABLE: {}\nVALUE:{}\n".format(key, value))
