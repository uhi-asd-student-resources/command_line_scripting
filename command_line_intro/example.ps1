# Ensure that Execution policy has been set
# Set-ExecutionPolicy RemoteSigned

Write-Output "Hello world"
Test-Connection www.google.com 
Write-Host -NoNewline "Press any key to continue..."
While ($KeyInfo.VirtualKeyCode -Eq $Null -Or $Ignore -Contains $KeyInfo.VirtualKeyCode) {
  $KeyInfo = $Host.UI.RawUI.ReadKey("NoEcho, IncludeKeyDown")
}
